# Attitude representation and propagation
The conventions used for attitude and angular velocities are defined below:
* A body-fixed frame, a Local-Vertical Local-Horizontal (LVLH), and a Earth-Centered Inertial frame are used and named (B), (L), and (N), respectively. The body frame is located at the spacecraft center of mass and is aligned with its principal axes. The ECI frame used is the one used by `libsgp4`.
* `xb` : Denotes a vector x expressed in the B frame.
* `qbn` : Denotes the quaternion from frame B to frame N.
* `Rbn` : Similarly to `qbn`, it denotes the rotation matrix from frame B to frame N. The representation of a vector `xb` (originally represented in the B frame, as the notation implies) in the N frame is obtained as `xn = Rbn * xb`

For the tumbling case, a random quaternion is returned.