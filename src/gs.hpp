/*
 *  satnogs-simulator: Satellite and Ground station network simulation tool
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once
#include "msg.hpp"
#include "params.hpp"
#include "satellite.hpp"
#include <deque>
#include <iostream>
#include <libsgp4/DateTime.h>
#include <mutex>
#include <spdlog/spdlog.h>
#include <vector>

class sat_link;

class gs
{
public:
  gs(const std::string &name, double lat, double lng, double elev,
     double min_horizon, params::log_format format);

  double
  latitude() const;

  double
  longitude() const;

  double
  elevation() const;

  double
  min_horizon() const;

  friend std::ostream &
  operator<<(std::ostream &output, const gs &G)
  {
    output << "Name        : " << G.m_name << std::endl;
    output << "Latitude    : " << G.m_lat << std::endl;
    output << "Longitude   : " << G.m_lon << std::endl;
    output << "Elevation   : " << G.m_elev << std::endl;
    return output;
  }

  void
  update(const DateTime &t, std::shared_ptr<spdlog::logger> logger);

  void
  register_link(std::shared_ptr<sat_link> l);

  size_t
  max_aos() const;

  void
  print_results() const;

  void
  recv_message(const satellite &sat);

private:
  std::string                            m_name;
  const double                           m_lat;
  const double                           m_lon;
  const double                           m_elev;
  const double                           m_min_horizon;
  const params::log_format               m_log_format;
  size_t                                 m_max_aos;
  size_t                                 m_downlink_cnt;
  std::vector<std::shared_ptr<sat_link>> m_links;

  std::string
  log_csv(const Observer &obs, const std::string &satname, const msg::sptr m);

  std::string
  log_json(const Observer &obs, const std::string &satname, size_t satid,
           const msg::sptr m);
};