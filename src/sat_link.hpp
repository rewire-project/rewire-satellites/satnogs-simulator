/*
 *  satnogs-simulator: Satellite and Ground station network simulation tool
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */
#pragma once

#include "gs.hpp"
#include "msg.hpp"
#include "satellite.hpp"
#include <deque>
#include <libsgp4/CoordGeodetic.h>
#include <libsgp4/CoordTopocentric.h>
#include <libsgp4/Observer.h>
#include <libsgp4/SGP4.h>
#include <memory>

class sat_link
{
public:
  using sptr = std::shared_ptr<sat_link>;

  sat_link(gs &gs, satellite &sat);

  const std::string &
  sat_name() const;

  size_t
  sat_id() const;

  bool
  aos();

  void
  downlink(msg::sptr msg);

  msg::sptr
  get_downlink();

private:
  gs                   &m_gs;
  satellite            &m_sat;
  Observer              m_observer;
  std::deque<msg::sptr> m_downlink_msgs;
};
