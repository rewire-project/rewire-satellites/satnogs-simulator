#include "anomalies.hpp"
#include "simulation.hpp"

anomalies::anomalies(const YAML::Node &config)
    : m_reset_prob(config["events"]["random-resets-prob"].as<double>()),
      m_ssa(config)
{
  /* For each satellite create the list of possible events */
  const auto &sats = config["events"]["satellites"];
  for (auto it = sats.begin(); it != sats.end(); it++) {
    try {
      const YAML::Node &node  = *it;
      size_t            norad = node["norad-id"].as<size_t>();
      if (node["tumbling"]) {
        m_events[norad].push_back(std::make_unique<tumbling>(
            node["tumbling"]["rate"].as<double>(),
            simulation::parse_iso8601_UTC(
                node["tumbling"]["start"].as<std::string>()),
            simulation::parse_iso8601_UTC(
                node["tumbling"]["stop"].as<std::string>())));
      }

      if (node["power-spike"]) {
        m_events[norad].push_back(std::make_unique<power_spike>(
            node["power-spike"]["consumption"].as<double>(),
            simulation::parse_iso8601_UTC(
                node["power-spike"]["start"].as<std::string>()),
            simulation::parse_iso8601_UTC(
                node["power-spike"]["stop"].as<std::string>())));
      }

      if (node["watchdog-reset"]) {
        m_events[norad].push_back(std::make_unique<wdg_reset>(
            node["watchdog-reset"]["period-secs"].as<size_t>(),
            simulation::parse_iso8601_UTC(
                node["watchdog-reset"]["start"].as<std::string>()),
            simulation::parse_iso8601_UTC(
                node["watchdog-reset"]["stop"].as<std::string>())));
      }
    } catch (...) {
    }
  }
}

const ssa &
anomalies::get_ssa() const
{
  return m_ssa;
}

double
anomalies::reset_prob() const
{
  return m_reset_prob;
}

const std::vector<event::uptr> &
anomalies::events(size_t satellite)
{
  std::scoped_lock lock(m_mtx);
  return m_events[satellite];
}
