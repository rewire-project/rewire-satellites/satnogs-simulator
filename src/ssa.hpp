/*
 *  satnogs-simulator: Satellite and Ground station network simulation tool
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <utility>
#include <vector>
#include <yaml-cpp/yaml.h>

class ssa
{
public:
  ssa(const YAML::Node &config);

  bool
  affected(const std::pair<double, double> &x, double alt = 0.0) const;

  double
  prob() const;

private:
  const bool                             m_enabled;
  const double                           m_prob;
  const double                           m_max_alt;
  std::vector<std::pair<double, double>> m_polygon;
};
