/*
 *  satnogs-simulator: Satellite and Ground station network simulation tool
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "gs.hpp"
#include "sat_link.hpp"
#include "simulation.hpp"
#include <nlohmann/json.hpp>

gs::gs(const std::string &name, double lat, double lng, double elev,
       double min_horizon, params::log_format format)
    : m_name(name),
      m_lat(lat),
      m_lon(lng),
      m_elev(elev),
      m_min_horizon(min_horizon),
      m_log_format(format),
      m_max_aos(0),
      m_downlink_cnt(0)
{
  std::replace(m_name.begin(), m_name.end(), ',', '-');
}

double
gs::latitude() const
{
  return m_lat;
}

double
gs::longitude() const
{
  return m_lon;
}

double
gs::elevation() const
{
  return m_elev;
}

double
gs::min_horizon() const
{
  return m_min_horizon;
}

void
gs::update(const DateTime &t, std::shared_ptr<spdlog::logger> logger)
{
  /* Update all registered links */
  auto observer = Observer(m_lat, m_lon, m_elev);
  for (auto i : m_links) {
    auto x = i->get_downlink();
    while (x) {

      switch (m_log_format) {
      case params::log_format::CSV:
        logger->info(log_csv(observer, i->sat_name(), x));
        break;
      case params::log_format::JSON:
        logger->info(log_json(observer, i->sat_name(), i->sat_id(), x));
        break;
      default:
        throw std::invalid_argument("Invalid output format");
      }
      m_downlink_cnt++;
      x = i->get_downlink();
    }
  }

  /* Update statistics */
  size_t cnt = std::count_if(m_links.cbegin(), m_links.cend(),
                             [](const sat_link::sptr l) { return l->aos(); });
  m_max_aos  = std::max(m_max_aos, cnt);
}

void
gs::register_link(sat_link::sptr l)
{
  m_links.push_back(l);
}

size_t
gs::max_aos() const
{
  return m_max_aos;
}

void
gs::print_results() const
{
  std::cout << "Name                                    :  " << m_name
            << std::endl;
  std::cout << "Max satellites at the field of view     :  " << max_aos()
            << std::endl;
  std::cout << "Frames received                         :  " << m_downlink_cnt
            << std::endl;
}

std::string
gs::log_csv(const Observer &obs, const std::string &satname, const msg::sptr m)
{
  auto q          = m->tlm().att().get_q();
  auto pwr        = m->tlm().pwr();
  auto pwr_panels = pwr.get_power_in_per_side();
  auto tmp        = obs;
  auto pos        = tmp.GetLookAngle(m->position());
  return satellite::to_string(m->timestamp()) + ", " + satname + ", " +
         std::to_string(
             Util::RadiansToDegrees(m->position().ToGeodetic().longitude)) +
         ", " +
         std::to_string(
             Util::RadiansToDegrees(m->position().ToGeodetic().latitude)) +
         ", " + std::to_string(m->position().ToGeodetic().altitude) + ", " +
         std::to_string(Util::RadiansToDegrees(pos.azimuth)) + ", " +
         std::to_string(Util::RadiansToDegrees(pos.elevation)) + ", " +
         std::to_string(pos.range) + ", " +
         std::to_string(Util::RadiansToDegrees(obs.GetLocation().latitude)) +
         ", " +
         std::to_string(Util::RadiansToDegrees(obs.GetLocation().longitude)) +
         ", " + std::to_string(q[0]) + ", " + std::to_string(q[1]) + ", " +
         std::to_string(q[2]) + ", " + std::to_string(q[3]) + ", " +
         std::to_string(pwr.bat().capacity()) + ", " +
         std::to_string(pwr.bat().critical()) + ", " +
         std::to_string(pwr.get_total_power_in()) + ", " +
         std::to_string(pwr_panels[0]) + ", " + std::to_string(pwr_panels[1]) +
         ", " + std::to_string(pwr_panels[2]) + ", " +
         std::to_string(pwr_panels[3]) + ", " + std::to_string(pwr_panels[4]) +
         ", " + std::to_string(pwr_panels[5]) + ", " +
         std::to_string(m->tlm().uptime_ms()) + ", " +
         std::to_string(m->tlm().reset_cnt()) + ", " + m_name + ", N/A";
}

std::string
gs::log_json(const Observer &obs, const std::string &satname, size_t satid,
             const msg::sptr m)
{
  auto q          = m->tlm().att().get_q();
  auto pwr        = m->tlm().pwr();
  auto pwr_panels = pwr.get_power_in_per_side();
  auto tmp        = obs;
  auto pos        = tmp.GetLookAngle(m->position());

  nlohmann::json influx;
  influx["time"]        = satellite::to_iso8601(m->timestamp());
  influx["measurement"] = satid;

  nlohmann::json tags;
  tags["satellite"] = satname;
  tags["decoder"]   = "satnogs-simulator";
  tags["station"]   = m_name;
  tags["observer"]  = "REWIRE";
  tags["source"]    = "satnogs-simulator";
  tags["version"]   = "1.0.0";

  influx["tags"] = tags;

  nlohmann::json j;
  j["Date"]      = satellite::to_string(m->timestamp());
  j["Satellite"] = satname;
  j["Satellite Longitude"] =
      Util::RadiansToDegrees(m->position().ToGeodetic().longitude);
  j["Satellite Latitude"] =
      Util::RadiansToDegrees(m->position().ToGeodetic().latitude);
  j["Satellite Altitude"] = m->position().ToGeodetic().altitude;
  j["Azimuth"]            = Util::RadiansToDegrees(pos.azimuth);
  j["Elevation"]          = Util::RadiansToDegrees(pos.elevation);
  j["Range"]              = pos.range;
  j["GS Longitude"]       = Util::RadiansToDegrees(obs.GetLocation().longitude);
  j["GS Latitude"]        = Util::RadiansToDegrees(obs.GetLocation().latitude);
  j["Attitude Q0"]        = q[0];
  j["Attitude Q1"]        = q[1];
  j["Attitude Q2"]        = q[2];
  j["Attitude Q3"]        = q[3];
  j["Battery Capacity"]   = pwr.bat().capacity();
  j["Battery Critical"]   = pwr.bat().critical();
  j["Total Power In"]     = pwr.get_total_power_in();
  j["Total In X+"]        = pwr_panels[0];
  j["Total In X-"]        = pwr_panels[1];
  j["Total In Y+"]        = pwr_panels[2];
  j["Total In Y-"]        = pwr_panels[3];
  j["Total In Z+"]        = pwr_panels[4];
  j["Total In Z-"]        = pwr_panels[5];
  j["Uptime"]             = m->tlm().uptime_ms();
  j["Resets"]             = m->tlm().reset_cnt();
  j["Event"]              = "N/A";
  j["Station"]            = m_name;
  j["Observer"]           = "REWIRE";

  influx["fields"] = j;

  return nlohmann::to_string(influx) + ",\n";
}
