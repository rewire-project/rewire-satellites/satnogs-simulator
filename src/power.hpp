/*
 *  satnogs-simulator: Satellite and Ground station network simulation tool
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once
#include "battery.hpp"
#include <eigen3/Eigen/Dense>
#include <libsgp4/Eci.h>
#include <libsgp4/SolarPosition.h>
#include <libsgp4/Vector.h>

#define R_EARTH 6371.0 // Earth radius [km]
#define G_NO                                                                   \
  1.361 // Solar irradiance outside the atmosphere, on a surface normal to the sun [kw/m^2]

class power
{
public:
  power(double bat_capacity, double bat_capacity_critical,
        double bat_capacity_safe, const Eigen::Vector<double, 6> &pv_area,
        const Eigen::Vector<double, 6> &pv_eff, double consumption);

  void
  set_consumption(double consumption);

  double
  consumption();

  double
  get_total_power_in();

  Eigen::Vector<double, 6>
  get_power_in_per_side();

  void
  calc_power_in(Vector V_pos_sat, Eigen::Vector4d qbn, const DateTime &dt);

  void
  update_battery_capacity(const DateTime &t);

  battery &
  bat();

private:
  Eigen::Vector<double, 6>
      m_power_in; /*!< Input power (instantaneous) from each side. Sides naming [x+, x-, y+, y-, z+, z-] */
  double                   m_oap_out; /*!< Orbit-averaged output power */
  Eigen::Vector<double, 6> m_pv_area; /*!< PV area on each side [mm^2] */
  Eigen::Vector<double, 6> m_pv_eff;  /*!< Efficiency of the PVs on each side */
  SolarPosition            m_sun;  /*!< Position of the sun in the ECI frame */
  battery                  m_batt; /*!< Battery object */
  bool                     m_first_update;
  DateTime                 m_timestamp;
};
