/*
 *  satnogs-simulator: Satellite and Ground station network simulation tool
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "sat_link.hpp"
#include <libsgp4/Util.h>

/**
 * @brief Get if AOS (Acquisition of Signal) exists
 *
 */
sat_link::sat_link(gs &gs, satellite &sat)
    : m_gs(gs),
      m_sat(sat),
      m_observer(gs.latitude(), gs.longitude(), gs.elevation())
{
}

const std::string &
sat_link::sat_name() const
{
  return m_sat.name();
}

size_t
sat_link::sat_id() const
{
  return m_sat.id();
}

bool
sat_link::aos()
{
  auto x = m_observer.GetLookAngle(m_sat.position());
  return Util::RadiansToDegrees(x.elevation) > m_gs.min_horizon();
}

void
sat_link::downlink(msg::sptr msg)
{
  if (aos()) {
    m_downlink_msgs.push_back(msg);
  }
}

msg::sptr
sat_link::get_downlink()
{
  if (m_downlink_msgs.empty()) {
    return nullptr;
  }
  auto x = m_downlink_msgs.front();
  m_downlink_msgs.pop_front();
  return x;
}
