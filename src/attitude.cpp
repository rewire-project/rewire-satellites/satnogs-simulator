/*
 *  satnogs-simulator: Satellite and Ground station network simulation tool
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "attitude.hpp"

attitude::attitude() : m_qbn({1.0, 0.0, 0.0, 0.0}), m_tumbling(false) {}

attitude::attitude(Eigen::Vector4d q) : m_qbn(q), m_tumbling(false) {}

void
attitude::set_q(const Eigen::Vector4d q)
{
  m_qbn = q;
}

Eigen::Vector4d
attitude::get_q() const
{
  return m_qbn;
}

void
attitude::set_tumbling(bool is_tumbling)
{
  m_tumbling = is_tumbling;
}

bool
attitude::get_tumbling()
{
  return m_tumbling;
}

/**
 * @brief Converts DCM to quaternion
 *
 * The algorithm is found in "Fundamentals of Spacecraft Attitude Determination and Control",
 * Markley and Crassidis, 2014 and aims to minimize numerical errors.
 *
 * @return Eigen::Vector4d
 */
Eigen::Vector4d
attitude::DCM_to_q(const Eigen::Matrix3d &DCM)
{
  // There are 4 possible, almost equal, quaternion vectors from a DCM and the goal
  // is to select the one which will produce the smallest numerical error.
  double tr_DCM = DCM(0, 0) + DCM(1, 1) + DCM(2, 2); // DCM trace

  Eigen::Vector4d tmp_quat; // to store temporary results - equals to 4*q_x*quat

  // Determine if the trace of the DCM is larger than the diagonal elements
  if (tr_DCM >= DCM(0, 0) && tr_DCM >= DCM(1, 1) && tr_DCM >= DCM(2, 2)) {
    tmp_quat << DCM(1, 2) - DCM(2, 1), DCM(2, 0) - DCM(0, 2),
        DCM(0, 1) - DCM(1, 0), 1.0 + tr_DCM;
    return tmp_quat / tmp_quat.norm();
  }

  // Determine which of the diagonal elements is the largest.
  // It does not matter if 2 or more of them are equal
  // could not use switch-case because max does not return integral type
  double max_diag_DCM = std::max({DCM(0, 0), DCM(1, 1), DCM(2, 2)});
  if (max_diag_DCM == DCM(0, 0)) {
    tmp_quat << 1.0 + 2.0 * DCM(0, 0) - tr_DCM, DCM(0, 1) + DCM(1, 0),
        DCM(0, 2) + DCM(2, 0), DCM(1, 2) - DCM(2, 1);
    return tmp_quat / tmp_quat.norm();
  } else if (max_diag_DCM == DCM(1, 1)) {
    tmp_quat << DCM(1, 0) + DCM(0, 1), 1.0 + 2.0 * DCM(1, 1) - tr_DCM,
        DCM(1, 2) + DCM(2, 1), DCM(2, 0) - DCM(0, 2);
    return tmp_quat / tmp_quat.norm();
  } else {
    tmp_quat << DCM(2, 0) + DCM(0, 2), DCM(2, 1) + DCM(1, 2),
        1.0 + 2.0 * DCM(2, 2) - tr_DCM, DCM(0, 1) - DCM(1, 0);
    return tmp_quat / tmp_quat.norm();
  }
}

Eigen::Matrix3d
attitude::q_to_DCM(const Eigen::Vector4d &q)
{
  // Unpack q for clarity
  double          q_0  = q[0];
  double          q_1  = q[1];
  double          q_2  = q[2];
  double          q_3  = q[3];
  Eigen::Vector3d q_13 = q.segment(1, 3);

  Eigen::Matrix3d I3, skew_q_13 = Eigen::MatrixXd::Zero(3, 3);
  I3.setIdentity();
  skew_q_13 << 0.0, -q_13[2], q_13[1], q_13[2], 0.0, -q_13[0], -q_13[1],
      q_13[0], 0.0;

  Eigen::Matrix3d DCM; // from the body frame to the ECI frame
  DCM = (2 * q_0 * q_0 - 1) * I3 + 2 * q_0 * skew_q_13 +
        2 * q_13 * q_13.transpose();

  return DCM;
}

/**
 * @brief Calculates the ratation matrix from the L to the N frame at some instant.
 *
 * The L frame is considered as follows:
 * - Z+ is towards the nadir vector (negative position vector)
 * - Y+ axis is in the negative orbit normal direction (negative orbit angular momentum vector)
 * - X+ completes the right-handed triad. In circular orbits, +X points towards the velocity vector
 *
 * @param pos
 * @param vel
 * @return Eigen::Matrix3d
 */
Eigen::Matrix3d
attitude::calc_L_frame(const Vector &pos, const Vector &vel)
{
  // Calculate the L frame vectors in the N frame
  Eigen::Vector3d Zl_n(-pos.x, -pos.y, -pos.z);
  Zl_n /= Zl_n.norm();

  Eigen::Vector3d Xl_n(vel.x, vel.y, vel.z);
  Xl_n /= Xl_n.norm();

  // For non-circular orbits, Zl_n and Xl_n as calculated above
  // are not perpendicular. Thus, a projection of Xl_n is calculated
  Xl_n = Xl_n - Zl_n.dot(Xl_n) * Zl_n;
  Xl_n /= Xl_n.norm();

  Eigen::Vector3d Yl_n;
  Yl_n = Zl_n.cross(Xl_n);

  // Construct the rotation matrix from the L frame to the N frame
  Eigen::Matrix3d Rln;
  Rln << Xl_n[0], Yl_n[0], Zl_n[0], Xl_n[1], Yl_n[1], Zl_n[1], Xl_n[2], Yl_n[2],
      Zl_n[2];

  return Rln;
}