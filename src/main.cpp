/*
 *  satnogs-simulator: Satellite and Ground station network simulation tool
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "argagg.hpp"
#include "simulation.hpp"
#include <chrono>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/spdlog.h>
#include <string>
#include <yaml-cpp/yaml.h>

using namespace std::chrono_literals;
using logger_ptr = std::shared_ptr<spdlog::logger>;

argagg::parser
setup_arg_parser()
{
  return argagg::parser{
      {{"help", {"-h", "--help"}, "shows this help message", 0},
       {"config",
        {"-c", "--config"},
        "File path of the configuration file",
        1}}};
}

argagg::parser_results
parse_args(argagg::parser &argparser, int argc, char **argv,
           logger_ptr console_logger)
{
  try {
    return argparser.parse(argc, argv);
  } catch (const std::exception &e) {
    console_logger->error("Exception: {}", e.what());
    exit(EXIT_FAILURE);
  }
}

int
check_args(argagg::parser_results &args, YAML::Node &config,
           logger_ptr console_logger)
{
  if (!args.has_option("config")) {
    console_logger->error("Configuration file not specified");
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

int
main(int argc, char **argv)
{
  auto console_logger = spdlog::stdout_color_mt("to_console");
  console_logger->set_pattern("%^[%Y-%m-%d %H:%M:%S.%e] [%l] %v%$");
  auto argparser = setup_arg_parser();
  auto args      = parse_args(argparser, argc, argv, console_logger);
  if (args["help"]) {
    std::ostringstream oss;
    oss << "Usage: program [options] ARG1 ARG2\n" << argparser;
    return EXIT_FAILURE;
  }
  YAML::Node config;
  if (args["config"]) {
    config = YAML::LoadFile(args["config"].as<std::string>());
  }
  auto sim = simulation(config);
  sim.start();
  return EXIT_SUCCESS;
}