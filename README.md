# satnogs-simulator: A simulation tool for the SatNOGS network

The satnogs-simulator is a CLI based tool, that simulates the SatNOGS network with
orbiting satellites.

The simulation parameters are specified through a YAML file.
Such a file can be found at the `contrib` directory.

Users can specify:
* The resolution of the simulation in miliseconds
* The start and stop date
* The tool makes use of a thread pool. Its size is also configurable
* Satellite information can be retrieved from the SatNOGS-DB. Custom
satellites are also supported by specifying the name and the TLE of the custom entry
* Ground station information can be retrived automatically from the SatNOGS network.
Again, custom ground stations can be defined too
* The simulation supports a basic transmission scheme for each satellite.
Messages are randommly "transmitted". Those messages are delivered to stations that
have a line of sight with the corresponding satellite. The line of site depends on the
ground station configured minimum horizon parameter.
* A set of events that can occur on the satellite while orbiting, are also supported.
Currently random reboots and reboots caused by the South-Atlantic-Anomaly are implemented

## Output Data
The simulation produces a single CSV file, containing the reception log and the events log as they occur on each spacecraft.
The log output file is specified by the `reception-log` parameter of the `YAML` configuration file.

To make easier the parsing of the file, all events that occur on the sattelite are artifically received by the `FAKE_STATION`.
This station acts like a governor of the entire simulation and can log events on all the satellites when they occur.
This for example is quite useful for instantaneous events (e.g reboots) that can occur outside of any field of view of the available ground stations and cannot be captured by periodic telemetry transmissions.

This logs contains telemetry from the satellite, plus some extra telemetry from the ground station when this particular frame was received (e.g ground station - satellite relative position).

|  | **Date** | **Satellite** | **Satellite Azimuth** | **Satellite Elevation** | **Satellite Range** | **GS Longitude** | **GS Latitude** | **Attitude Q[0]** | **Attitude Q[1]** | **Attitude Q[2]** | **Attitude Q[3]** | **Battery Capacity (mWh)** | **Battery Critical** | **Solar Panel Total (mW)** | **Solar Panel X+ (mW)** | **Solar Panel X- (mW)** | **Solar Panel Y+ (mW)** | **Solar Panel Y- (mW)** | **Solar Panel Z+ (mW)** | **Solar Panel Z- (mW)** | **Satellite Uptime (ms)** | **Satellite Resets** | **Ground Station** | **Event** |
|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
|  | Frame reception time in UTC | Satellite name | Satellite azimuth from the GS perspective | Satellite elevation from the GS perspective | Distance between the GS and the satellite | The longitude of the receiving ground station | The latitude of the receiving ground station | Q[0] quaternion indicating the orientation of the body with respect to earth. Refer to “Fundamentals of Spacecraft Attitude Determination and Control” for more details | Q[1] quaternion indicating the orientation of the body with respect to earth. Refer to “Fundamentals of Spacecraft Attitude Determination and Control” for more details | Q[2] quaternion indicating the orientation of the body with respect to earth. Refer to “Fundamentals of Spacecraft Attitude Determination and Control” for more details | Q[3] quaternion indicating the orientation of the body with respect to earth. Refer to “Fundamentals of Spacecraft Attitude Determination and Control” for more details | Battery capacity in mWh | Indicates if the satellite have reached a battery critical state | Power from all available panels | Power from the X+ axis solar panel | Power from the X- axis solar panel | Power from the Y+ axis solar panel | Power from the Y- axis solar panel | Power from the Z+ axis solar panel | Power from the Z- axis solar panel | Satellite uptime since last reset | Satellite reset counter | Ground station that received this frame | The type of the event |
| **Remarks** |  |  |  |  | 0.0 for the FAKE_STATION | 0.0 for the FAKE_STATION | 0.0 for the FAKE_STATION |  |  |  |  |  | When the satellite reaches the battery critical state, it disables all non-vital subsystems in an effort to reduce the consumption. The satellite stays at this state until the battery level reaches a normal level again. |  |  |  |  |  |  |  |  |  | Should be FAKE_STATION for the artificial GS acting as the governor of the simulation | For normal stations this should be N/A, because they don't have exact knowledge of what happens in the satellite. Also normal periodic telemetry is not able to catch instantaneous events like a reset for example. On the other hand, FAKE_STATION fills this field at each time slice of the simulation the status of the satellite. |


## Requirements

To build the `satnogs-simulator` the following software packages should be available:

* CMake (>= 3.20)
* C++17
* GNU Make
* spdlog
* cpprest
* boost-random
* yaml-cpp
* eigen3

To install the dependencies in Debian based distros, you can use the following commands:
```bash
sudo apt-get install -qy --no-install-recommends git ca-certificates cmake cmake-extras make gcc gcc-multilib g++-multilib  libcpprest-dev libboost-system-dev libboost-random-dev libspdlog-dev libeigen3-dev
```

To build the tool use the commands below:
```bash
cd satnogs-simulator
mkdir build
cd build
cmake ..
make
```

## Performance
The architecture of the simulation tool has been optimized for perfomance and speed
trying to reduce the critical sections between trheads.

> [!WARNING]
> There is a known issue with the REST API of SatNOGS, being to aggressive on the throttling.
> This will delay considerably the fetching of satellites and especially the ground stations.

## Uploading to the SatNOGS influxDB
The simoulation provides the ability to store the results in a JSON form compatible with the influxdb schema.
You can use the `satnogs_upload.py` utility, that is inside the `contrib/scripts` directory to upload them.


## License
![Libre Space Foundation](docs/assets/LSF_HD_Horizontal_Color1-300x66.png)
&copy; 2024 [Libre Space Foundation](https://libre.space).

