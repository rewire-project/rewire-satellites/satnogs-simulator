import argparse
from influxdb import InfluxDBClient
import json

# Configuration
influx_host = 'warehouse.satnogs.org'
influx_port = 8086
influx_user = 'test'  # Replace with your InfluxDB username
influx_password = 'test'  # Replace with your InfluxDB password
influx_dbname = 'test'  # Replace with your InfluxDB database name

def post_json_to_influxdb(json_file_path):
    # Initialize InfluxDB client
    client = InfluxDBClient(
        host=influx_host,
        port=influx_port,
        username=influx_user,
        password=influx_password,
        database=influx_dbname,
        ssl=True,
        verify_ssl=True
    )

    # Read JSON data from file
    with open(json_file_path, 'r') as file:
        json_data = json.load(file)

    # Ensure the data is in the correct format for InfluxDB
    # The format is a list of dictionaries, where each dictionary represents a point
    # Example:
    # [
    #     {
    #         "measurement": "cpu_load_short",
    #         "tags": {
    #             "host": "server01",
    #             "region": "us-west"
    #         },
    #         "time": "2009-11-10T23:00:00Z",
    #         "fields": {
    #             "value": 0.64
    #         }
    #     }
    # ]

    # Convert the JSON data to the InfluxDB format if necessary
    # This step depends on the structure of your JSON file

    # Write data to InfluxDB
    try:
        client.write_points(json_data, batch_size=32)
        print("Data successfully written to InfluxDB.")
    except Exception as e:
        print(f"Failed to write data to InfluxDB: {e}")

if __name__ == '__main__':
    # Set up command-line argument parsing
    parser = argparse.ArgumentParser(description="Post a JSON file to InfluxDB.")
    parser.add_argument('json_file', type=str, help="Path to the JSON file to be posted to InfluxDB")

    # Parse the command-line arguments
    args = parser.parse_args()

    # Call the function with the provided JSON file
    post_json_to_influxdb(args.json_file)
